
--------------------------------------------------------------------
-- Topics 2 - 2012/2013
-- File: Quilt.hs
-- Contents: Simple quilt processing functions
--------------------------------------------------------------------           

module Quilt where

data Quilt = Qt [String]

instance Show Quilt where
	showsPrec _ (Qt []) = ('\n':)
	showsPrec i (Qt (l:ls)) = ('\n':) . (l++) . showsPrec i (Qt ls)

plain :: Int -> Int -> Quilt
fancy :: Char -> Char -> Int -> Quilt
flipV :: Quilt -> Quilt
flipH :: Quilt -> Quilt
rotate :: Quilt -> Quilt
sewV :: Quilt -> Quilt -> Quilt
sewH :: Quilt -> Quilt -> Quilt
width :: Quilt -> Int
height :: Quilt -> Int
border :: Char -> Quilt -> Quilt

plain n m = Qt [['.'|i<-[1..n]] | j<-[1..m]]

fancy c1 c2 n
     = Qt [(copy c1 (j-1))++['.']++(copy c2 (n-j)) | j<-[1..n] ]

flipV (Qt q) = Qt (reverse q)
flipH (Qt q) = Qt (map reverse q)

sewH (Qt p) (Qt q)
 =	if height (Qt p) == height (Qt q)
	then Qt (zipwith (++) p q)
	else error "cannot join quilts of different height"

sewV (Qt p) (Qt q)
 =	if width (Qt p) == width (Qt q)
	then Qt (p++q)
	else error "cannot join quilts of different width"

width  (Qt p) = length (head p)
height (Qt p) = length p

border c (Qt q) = Qt ([cc]++(map wrap q)++[cc])
            	  where
            	  cc = copy c (width (Qt q) + 2)
            	  wrap l = [c]++l++[c]

rotate (Qt q) = Qt (foldr (zipwith (:)) (copy [] (width (Qt q))) (reverse q))

copy c n = [c|i<-[1..n]]
zipwith f l k = [f x y| (x,y)<- zip l k]
rows (Qt ls) = ls


