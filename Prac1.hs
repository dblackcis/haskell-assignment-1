--------------------------------------------------------------------
-- Toics2: Programming with Simple Functions 
-- File: Prac1.hs
-- Created: 14/01/2013                                                
-- Author: Neil Ghani
-- Contents: Exercises and problems in functional programming
--------------------------------------------------------------------           
-- David Black - 201143622
-- Barry McAuley - 201106816
--------------------------------------------------------------------

module Prac1 where 

import Quilt

--------------------------------------------------------------------
-- Examples of quilts
--------------------------------------------------------------------

eg :: Int -> Quilt

eg 1     = plain 10 9
eg 2     = fancy '%' '@' 10
eg 3     = sewV (eg 2) (eg 1)
eg 4	 = rotate (eg 2)
eg 5 	 = (rotate . rotate) (eg 2)
eg 6     = rotate (rotate (eg 2))
eg other = error "No such example"



--------------------------------------------------------------------
-- Answers
--------------------------------------------------------------------

triangle :: Int -> Quilt
triangle n = fancy '$' '-' n

pile :: Int -> Quilt
pile n = (rotate . rotate . rotate) (sewV tri (rotate(tri)))
          where
          tri = triangle n

stripe :: Int -> Quilt
stripe n = sewH q (rotate (rotate q))
         where
         q = fancy '.' '$' n

thrice :: (a -> a) -> a -> a
thrice f x = f(f(f(x)))

moneypiles :: Quilt
moneypiles = border '#' (border '#' (sewV bp tp))
		 where
		 p = pile 5
		 tp =  sewH(sewH (plain 5 5) p) (sewH p p)
		 bp = sewH(sewH(sewH p p) p) (plain 5 5)
		 
foursome :: Quilt -> Quilt
foursome q = sewH fhalf shalf
		 where
		 fhalf = sewV q (thrice rotate q)
		 shalf = sewV (rotate q) ( (rotate . rotate) q)
		 
windmill :: Quilt
windmill = foursome(triangle 5)

diamond :: Quilt
diamond = sewH(sewV((rotate . rotate . rotate) tri) ((rotate . rotate) tri)) (sewV tri (rotate tri))
		 where
		 tri = triangle 10


-- if width of quilt is less than its height, place the patch on the left
-- if width of quilt is greater than its height, place the patch on top
squareup :: Quilt -> Quilt
squareup q = if width (q) < height (q)
				 then sewH(plain(height (q) - width (q)) (height q)) (q)
					 else sewV(plain (width (q)) (width (q) - height (q))) (q)
			 			 
flower :: Int -> Quilt
flower n = foursome (squareup (stripe n))